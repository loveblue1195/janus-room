export const environment = {
  production: true,
  wss: 'wss://forward-livestream-stage.panelist.com',
  wssJanus: 'wss://demo-call.panelist.com:8188/janus'
};
