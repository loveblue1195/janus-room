import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import Room from 'janus-room';
import { BehaviorSubject } from 'rxjs';
// declare var Room
@Injectable({
  providedIn: 'root'
})
export class RoundTableService {
  publishOwnFeed = true;
  room;
  roomId = ''
  username = ''
  joinedRoom = new BehaviorSubject({ isJoin: false })
  feedId = 0
  constructor() { }

  /**
   *  Connect room janus
   * @param roomId 
   * @param username 
   */
  connectRoom(roomId, username, token) {
    this.roomId = roomId
    this.username = username
    const options = {
      server: environment.wssJanus,
      room: roomId,
      token: token,
      extensionId: 'bkkjmbohcfkfemepmepailpamnppmjkk',
      publishOwnFeed: this.publishOwnFeed,
      iceServers: [{ urls: 'stun:stun.l.google.com:19302' }],
      useRecordPlugin: true,
      volumeMeterSkip: 10,
      onLocalJoin: this.onLocalJoin.bind(this),
      onRemoteJoin: this.onRemoteJoin.bind(this),
      onRemoteUnjoin: this.onRemoteUnjoin.bind(this),
      onRecordedPlay: this.onRecordedPlay.bind(this),
      onMessage: this.onMessage.bind(this),
      onError: this.onError.bind(this),
      onWarning: this.onWarning.bind(this),
      onVolumeMeterUpdate: this.onVolumeMeterUpdate.bind(this),
    }
    this.room = new Room(options)
    this.room.init().then(() => {
      setTimeout(() => {
        this.room.register({
          username: username,
          room: roomId
        })
      }, 1000);
    }).catch((err) => {
      console.log('connect room error', err);
    });
  }

  /**
   * Share screen
   */
  async shareScreen() {
    await this.room.shareScreen().then(() => {
    }).catch((err) => {
      console.log('share screen error', err);
    });
  }

  /**
   * Stop Share screen
   */
  stopShareScreen() {
    this.room.stopShareScreen().then((result) => {
      console.log('stopped share');
    }).catch((err) => {
      console.log('stop share screen error', err);
    });
  }

  removeRoom() {
    this.room.removeRoom().then(() => {
      setTimeout(() => {
        this.room.stop()
      }, 1000);
    }).catch((err) => {
      console.log('remove room error', err);
    });
  }

  joinToRoom(username) {
    this.room.register({
      username
    })
  }

  toggleAudio() {
    this.room.toggleMuteAudio().then((muted) => {
      var el = document.getElementById('local-toggle-mute-audio');
      if (muted) {
        el.innerHTML = "Unmute";
      } else {
        el.innerHTML = "Mute";
      }
    })
  }

  toggleVideo() {
    this.room.toggleMuteVideo().then((muted) => {
      var el = document.getElementById('local-toggle-mute-video');
      if (muted) {
        el.innerHTML = "Unmute";
      } else {
        el.innerHTML = "Mute";
      }
    })
  }

  onError(err) {
    const that = this;
    if (err.indexOf('The room is unavailable') > -1) {
      alert('Room is unavailable. Let\'s create one.');
      this.room.createRoom({
        room: this.roomId
      })
        .then(() => {
          setTimeout(function () {
            that.room.register({
              username: that.username,
              room: that.roomId
            });
          }, 1000);
        })
        .catch((err) => {
          // alert(err);
          console.log('janus room error', err);
        })
    } else {
      console.log('janus room errors', err);
    }
  }


  onWarning(msg) {
    console.log('warning', msg);
  }

  onVolumeMeterUpdate(streamIndex, volume) {
    let el = document.getElementById('volume-meter-0');
    el.style.width = volume + '%';
  }

  onLocalJoin() {
    this.joinedRoom.next({ isJoin: true })
    let htmlStr = '<video id="myvideo" autoplay muted="muted" style="width: 600px" />';
    document.getElementById('videolocal').innerHTML = htmlStr;
    let target = document.getElementById('myvideo');
    console.log('room', this.room);
    this.room.attachStream(target, 0);
  }

  onRemoteJoin(index, remoteUsername, feedId) {
    console.log('feedId', feedId);
    if (feedId !== this.feedId && remoteUsername !== 'panelist_bot') {
      this.feedId = feedId;
      const videoRemote = document.getElementById('videoRemoted')
      const div = document.createElement('div')
      div.id = `videoremote${index}`
      div.classList.add('col-sm')
      videoRemote.appendChild(div)
      document.getElementById('videoremote' + index).innerHTML = '<div>' + remoteUsername + ':' + feedId + '</div><video id="remotevideo' + index + '" style="width: 300px" autoplay/>';
      let target = document.getElementById('remotevideo' + index);
      this.room.attachStream(target, index);
    }
  }

  onRemoteUnjoin(index) {
    document.getElementById('videoremote' + index).innerHTML = '<div>videoremote' + index + '</div>';
  }

  onRecordedPlay() {
    var htmlStr = '<div>playback</div>';
    htmlStr += '<video id="playback" autoplay muted="muted"/>';
    document.getElementById('videoplayback').innerHTML = htmlStr;
    let target = document.getElementById('playback');
    this.room.attachRecordedPlayStream(target);
  }

  onMessage(data) {
    if (!data) {
      return;
    }
    if (data.type && data.type === 'chat') {
      document.getElementById("chatbox").innerHTML += '<p>' + data.sender + ' : ' + data.message + '</p><hr>';
    } else if (data.type && data.type === 'request') {
      if (data.action && data.action === 'muteAudio') {
      }
    }
  }


}
