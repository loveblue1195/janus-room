import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LiveStremingService {
  ws: WebSocket
  recorder;
  stopCapture = new BehaviorSubject({ stopCapture: false })
  constructor() { }

  connectWebsocket(token) {
    const url = `${environment.wss}/rtmp?key=livestream&token=${token}&useragent=chorme`
    this.ws = new WebSocket(url)
    this.ws.addEventListener('open', () => {
      console.log('socket connected');
    })
    this.ws.addEventListener('close', () => {
      console.log('socket connect closed');
    })
    this.ws.addEventListener('error', (e) => {
      console.log('socket error:', e);
    })
    // this.ws.addEventListener('')
  }

  /**
  * Mixes multiple audio tracks and the first video track it finds
  * */
  mixerStream(stream1, stream2) {
    const ctx = new AudioContext();
    const dest = ctx.createMediaStreamDestination()
    if (stream1.getAudioTracks().length > 0)
      ctx.createMediaStreamSource(stream1).connect(dest)
    if (stream2.getAudioTracks().length > 0)
      ctx.createMediaStreamSource(stream2).connect(dest)

    let tracks = dest.stream.getTracks()
    tracks = tracks.concat(stream1.getVideoTracks()).concat(stream2.getVideoTracks())
    return new MediaStream(tracks)
  }

  /**
   * Start record streaming
   */
  async startRecording() {
    let gumStream, gdmStream;
    let recordingData = [];
    console.log('love');
    try {
      gumStream = await navigator.mediaDevices.getUserMedia({ video: false, audio: true })
      // @ts-ignore
      gdmStream = await navigator.mediaDevices.getDisplayMedia({ video: { displaySurface: 'browser' }, audio: true })
    } catch (e) {
      console.log('capture failure', e);
      return
    }

    console.log(gumStream);

    const recordStream = gumStream ? this.mixerStream(gumStream, gdmStream) : gdmStream;
    // @ts-ignore
    this.recorder = new MediaRecorder(recordStream, { mimeType: 'video/webm', videoBitsPerSecond: 3 * 1024 * 1024 })
    this.recorder.ondataavailable = e => {
      if (e.data && e.data.size > 0) {
        console.log('live streaming packet', e);
        this.ws.send(e.data)
      }
    }

    this.recorder.onStop = () => {
      recordStream.getTracks().forEach(track => track.stop());
      gumStream.getTracks().forEach(track => track.stop())
      gdmStream.getTracks().forEach(track => track.stop())
      this.ws.close()
    }

    recordStream.addEventListener('inactive', () => {
      console.log('Capture stream inactive');
      // stop capture
      this.stopCapture.next({ stopCapture: true })
      this.recorder.stop()
    })

    this.recorder.start(1000)
    console.log("started recording");
  }

  /**
   * Stop Record streaming
   */
  stopRecordStream() {
    this.recorder.stop()
  }
}
