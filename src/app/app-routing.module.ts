import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RoundTableComponent } from './components/round-table/round-table.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'round-table',
    component: RoundTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
